"""Permet d'éteindre ou non le Raspberry
Cette page apparait si l'utilisateur sélectionne la page "Eteindre" sur la page d'accueil
"""

from sys import path
from os import system
from time import sleep

from abstractPage import AbstractPage
from classPages import Pages
from classBtnEvent import BtnEvent

path.append('/home/pi/polar-led-project/djangotest/djangotest/Fichier_commun')

from functions import line
from constante import XABSCISSA, FONT

class Eteindre(AbstractPage):

    index = 1
    shutdown = 0

    def render(self, draw):
        # Retour au menu
        if self.index == 1:
            draw.text((XABSCISSA, line(0)), 'Eteindre le Raspberry ?', font=FONT, fill=255)
            draw.text((XABSCISSA, line(1)), '> Non', font=FONT, fill=255)
            draw.text((XABSCISSA, line(2)), 'Oui', font=FONT, fill=255)

        # Arrêt du Raspberry
        elif self.index == 2:
            draw.text((XABSCISSA, line(0)), 'Eteindre le Raspberry ?', font=FONT, fill=255)
            draw.text((XABSCISSA, line(1)), 'Non', font=FONT, fill=255)
            draw.text((XABSCISSA, line(2)), '> Oui', font=FONT, fill=255)

        elif self.shutdown == 1:
            draw.text((XABSCISSA,line(0)), 'Attendez que la LED', font=FONT, fill=255)
            draw.text((XABSCISSA,line(1)), 'verte du Raspberry ne ', font=FONT, fill=255)
            draw.text((XABSCISSA,line(2)), 'clignote plus avant', font=FONT, fill=255)
            draw.text((XABSCISSA,line(3)), "de couper", font=FONT, fill=255)
            draw.text((XABSCISSA,line(4)), "l'alimentation", font=FONT, fill=255)
            self.shutdown = 1
            
    def getNextPage(self, event):
        # Sélection du choix
        if event == BtnEvent.DOWN:
            self.index += 1
            if self.index == 3:
                self.index = 1

        elif event == BtnEvent.UP:
            self.index -= 1
            if self.index == 0:
                self.index = 2

        # Validation du choix sélectionné
        if (self.index == 1 and event == BtnEvent.ENTER):
            return Pages.ACCUEIL

        elif (self.index == 2 and event == BtnEvent.ENTER):
            self.index = 0
            self.shutdown = 1

        elif self.shutdown == 1:
            sleep(0.5)
            system('sudo shutdown -h now')
            
        return Pages.ETEINDRE
