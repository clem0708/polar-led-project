"""Page permettant à l'utilisateur de modifier l'adresse IP du Raspberry
Cette page apparait une fois que l'utilisateur a fini de lire le fichier d'aide sur la modification de l'adresse IP
"""

from sys import path
from time import sleep

from abstractPage import AbstractPage
from classPages import Pages
from classBtnEvent import BtnEvent

path.append('/home/pi/polar-led-project/djangotest/djangotest/Fichier_commun')

from functions import line, formatIP, affValSet, valideIP
from constante import XABSCISSA, FONT

class ModificationIP(AbstractPage):

    # Déclaration des variables
    val = 0
    index = 1
    verrou = False

    # Adresse IP par défaut : 192.168.056.101
    ipOct1List = [1, 9, 2]
    ipOct2List = [1, 6, 8]
    ipOct3List = [0, 5, 6]
    ipOct4List = [1, 0, 1]

    def render(self, draw):
        
        # Affichage de l'adresse IP par défaut
        formatIP(draw, self.ipOct1List, self.ipOct2List, self.ipOct3List, self.ipOct4List)

        # Mode sélection de champ
        if self.verrou == False:
            # Ajout d'un repère visuel sous le champ que l'on souahite sélectionner
            if self.index == 1:
                draw.text((XABSCISSA, line(1)), '^', font=FONT, fill=255)
                draw.text((XABSCISSA, line(3)), 'Valider', font=FONT, fill=255)
                draw.text((XABSCISSA, line(4)), "Retour a l'aide", font=FONT, fill=255)

            elif self.index == 2:
                draw.text((XABSCISSA+6, line(1)), '^', font=FONT, fill=255)
                draw.text((XABSCISSA, line(3)), 'Valider', font=FONT, fill=255)
                draw.text((XABSCISSA, line(4)), "Retour a l'aide", font=FONT, fill=255)

            elif self.index == 3:
                draw.text((XABSCISSA+12, line(1)), '^', font=FONT, fill=255)
                draw.text((XABSCISSA, line(3)), 'Valider', font=FONT, fill=255)
                draw.text((XABSCISSA, line(4)), "Retour a l'aide", font=FONT, fill=255)

            elif self.index == 4:
                draw.text((XABSCISSA+24, line(1)), '^', font=FONT, fill=255)
                draw.text((XABSCISSA, line(3)), 'Valider', font=FONT, fill=255)
                draw.text((XABSCISSA, line(4)), "Retour a l'aide", font=FONT, fill=255)

            elif self.index == 5:
                draw.text((XABSCISSA+30, line(1)), '^', font=FONT, fill=255)
                draw.text((XABSCISSA, line(3)), 'Valider', font=FONT, fill=255)
                draw.text((XABSCISSA, line(4)), "Retour a l'aide", font=FONT, fill=255)

            elif self.index == 6:
                draw.text((XABSCISSA+36, line(1)), '^', font=FONT, fill=255)
                draw.text((XABSCISSA, line(3)), 'Valider', font=FONT, fill=255)
                draw.text((XABSCISSA, line(4)), "Retour a l'aide", font=FONT, fill=255)

            elif self.index == 7:
                draw.text((XABSCISSA+48, line(1)), '^', font=FONT, fill=255)
                draw.text((XABSCISSA, line(3)), 'Valider', font=FONT, fill=255)
                draw.text((XABSCISSA, line(4)), "Retour a l'aide", font=FONT, fill=255)

            elif self.index == 8:
                draw.text((XABSCISSA+54, line(1)), '^', font=FONT, fill=255)
                draw.text((XABSCISSA, line(3)), 'Valider', font=FONT, fill=255)
                draw.text((XABSCISSA, line(4)), "Retour a l'aide", font=FONT, fill=255)

            elif self.index == 9:
                draw.text((XABSCISSA+60, line(1)), '^', font=FONT, fill=255)
                draw.text((XABSCISSA, line(3)), 'Valider', font=FONT, fill=255)
                draw.text((XABSCISSA, line(4)), "Retour a l'aide", font=FONT, fill=255)

            elif self.index == 10:
                draw.text((XABSCISSA+72, line(1)), '^', font=FONT, fill=255)
                draw.text((XABSCISSA, line(3)), 'Valider', font=FONT, fill=255)
                draw.text((XABSCISSA, line(4)), "Retour a l'aide", font=FONT, fill=255)

            elif self.index == 11:
                draw.text((XABSCISSA+78, line(1)), '^', font=FONT, fill=255)
                draw.text((XABSCISSA, line(3)), 'Valider', font=FONT, fill=255)
                draw.text((XABSCISSA, line(4)), "Retour a l'aide", font=FONT, fill=255)

            elif self.index == 12:
                draw.text((XABSCISSA+84, line(1)), '^', font=FONT, fill=255)
                draw.text((XABSCISSA, line(3)), 'Valider', font=FONT, fill=255)
                draw.text((XABSCISSA, line(4)), "Retour a l'aide", font=FONT, fill=255)

            elif self.index == 13:
                draw.text((XABSCISSA, line(3)), '> Valider', font=FONT, fill=255)
                draw.text((XABSCISSA, line(4)), "Retour a l'aide", font=FONT, fill=255)

            elif self.index == 14:
                draw.text((XABSCISSA, line(3)), 'Valider', font=FONT, fill=255)
                draw.text((XABSCISSA, line(4)), "> Retour a l'aide", font=FONT, fill=255)

        elif self.verrou == True:
            
            if self.index >= 1 and self.index <= 3:
                affValSet(draw, self.val, self.index)            

            elif self.index >= 4 and self.index <= 6:
                affValSet(draw, self.val, self.index)

            elif self.index >= 7 and self.index <= 9:
                affValSet(draw, self.val, self.index)

            elif self.index >= 10 and self.index <= 12:
                affValSet(draw, self.val, self.index)

    def getNextPage(self, event):
         
        # Retour à l'accueil en ignorant les modifications
        if event == BtnEvent.MENU:
            self.val = 0
            self.index = 1
            self.verrou = False
            self.ipOct1List = [1, 9, 2]
            self.ipOct2List = [1, 6, 8]
            self.ipOct3List = [0, 5, 6]
            self.ipOct4List = [1, 0, 1]
            return Pages.ACCUEIL

        # ----------------------Mode sélection-------------------------
        elif self.verrou == False:
        
            # Changement de mode (sélection vers édition)
            if (self.index >= 1 and self.index <= 12) and event == BtnEvent.ENTER:
                
                # Choix de la bonne liste
                if self.index >= 1 and self.index <= 3:
                    self.val = self.ipOct1List[self.index - 1]
                
                elif self.index >= 4 and self.index <= 6:
                    self.val = self.ipOct2List[self.index - 4]

                elif self.index >= 7 and self.index <= 9:
                    self.val = self.ipOct3List[self.index - 7]

                elif self.index >= 10 and self.index <= 12:
                    self.val = self.ipOct4List[self.index - 10]

                self.verrou = True
                sleep(.2)
        
            # Sélection du champ
            elif event == BtnEvent.UP:
                self.index -= 1

                if self.index == 0:
                    self.index = 14

            elif event == BtnEvent.DOWN:
                self.index += 1

                if self.index == 15:
                    self.index = 1

            # Application des changements
            elif self.index == 13 and event == BtnEvent.ENTER:
                valideIP(self.ipOct1List, self.ipOct2List, self.ipOct3List, self.ipOct4List)
                self.val = 0
                self.index = 1
                self.verrou = False
                self.ipOct1List = [1, 9, 2]
                self.ipOct2List = [1, 6, 8]
                self.ipOct3List = [0, 5, 6]
                self.ipOct4List = [1, 0, 1]
                return Pages.AIDEMASQUE

            # Retour à la page d'aide
            elif self.index == 14 and event == BtnEvent.ENTER:
                return Pages.AIDEIP
        
        #---------------------Mode édition---------------------------
        elif self.verrou == True:
            
            # Changement de mode (édition vers sélection)
            if event == BtnEvent.ENTER:
                # Choix de la bonne liste
                if self.index >= 1 and self.index <= 3:
                    self.ipOct1List[self.index - 1] = self.val
                
                elif self.index >= 4 and self.index <= 6:
                    self.ipOct2List[self.index - 4] = self.val

                elif self.index >= 7 and self.index <= 9:
                    self.ipOct3List[self.index - 7] = self.val

                elif self.index >= 10 and self.index <= 12:
                    self.ipOct4List[self.index - 10] = self.val
                
                self.verrou = False
                sleep(.2)
            
            # Modification de la valeur du champ sélectionné
            elif event == BtnEvent.UP:
                self.val -= 1

                if self.val == -1:
                    self.val = 9
            
            elif event == BtnEvent.DOWN:
                self.val += 1
                
                if self.val == 10:
                    self.val = 0
        
        return Pages.MODIFIP