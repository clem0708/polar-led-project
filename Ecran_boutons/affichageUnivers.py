"""Page affichant l'univers du Raspberry
"""

from sys import path
from json import load

from abstractPage import AbstractPage
from classPages import Pages
from classBtnEvent import BtnEvent

path.append('/home/pi/polar-led-project/djangotest/djangotest/Fichier_commun')

from functions import line

from constante import XABSCISSA, FONT, DATA

class AffichageUnivers(AbstractPage):

    with open(DATA, 'r') as file:
        data = load(file)

    def render(self, draw):
        draw.text((XABSCISSA, line(0)), 'Univers du Raspberry :', font=FONT, fill=255)
        draw.text((XABSCISSA, line(1)), self.data["univers"], font=FONT, fill=255)
        draw.text((XABSCISSA, line(4)), '"Menu" pour quitter', font=FONT, fill=255)

    def getNextPage(self, event):
        if (event == BtnEvent.MENU):
            return Pages.ACCUEIL
        return Pages.AFFUNIVERS