"""Cette classe permet d'associer une page renseigner présente le tableau les regroupant dans le Page Manager avec l'énumération correspondante
"""

from enum import Enum

class Pages(Enum):
    # Pages générales
    INITDATA = 0 # Page initialisant le fichier data.json
    ACCUEIL = 1 # Page d'accueil
    ETEINDRE = 2 # Page d'extinction
    
    # Page en lien avec l'adresse IP
    AFFIP = 3 # Page pour afficher l'IP
    INTERFACE = 4 # Page pour modifier l'interface
    AIDEIP = 5 # Page d'aide pour la modification de l'adresse IP
    MODIFIP = 6 # Page d'aide pour changer IP
    AIDEMASQUE = 7 # Page d'aide pour modifier le masque
    MODIFMASQUE = 8 # Page pour changer le masque
    CONFMODIF = 9 # Page pour appliquer la modification de l'adresse IP + masque

    # Page en lien avec le masque
    AFFUNIVERS = 10 # Page pour afficher l'univers
    AIDEUNIVERS = 11 # Page d'aide pour la modification de l'univers
    MODIFUNIVERS = 12 # Page pour changer l'univers