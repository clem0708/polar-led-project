"""Page affichant l'adresse IP du Raspberry
"""

from sys import path
from json import load

from abstractPage import AbstractPage
from classPages import Pages
from classBtnEvent import BtnEvent

path.append('/home/pi/polar-led-project/djangotest/djangotest/Fichier_commun')

from functions import line, getIp

from constante import XABSCISSA, FONT

class AffichageIP(AbstractPage):

    def render(self, draw):
        draw.text((XABSCISSA, line(0)), 'IP Raspberry :', font=FONT, fill=255)
        draw.text((XABSCISSA, line(1)), str(getIp()), font=FONT, fill=255)
        draw.text((XABSCISSA, line(4)), '"Menu" pour quitter', font=FONT, fill=255)

    def getNextPage(self, event):
        if (event == BtnEvent.MENU):
            return Pages.ACCUEIL
        return Pages.AFFIP