"""Page d'accueil : Affiche la liste des pages
                        - Affichage de l'adresse IP
                        - Modification de l'adresse IP
                        - Affichage de l'univers
                        - Modification de l'univers
                        - Extinction du Raspberry
"""

from sys import path
from time import sleep

from abstractPage import AbstractPage
from classPages import Pages
from classBtnEvent import BtnEvent

path.append('/home/pi/polar-led-project/djangotest/djangotest/Fichier_commun')

from functions import line
from constante import XABSCISSA, FONT

class Accueil(AbstractPage):
    index = 1

    def render(self, draw):
        # Affichage de l'adresse IP
        if self.index == 1:
            draw.text((XABSCISSA, line(0)), '> Adresse IP', font=FONT, fill=255)
            draw.text((XABSCISSA, line(1)), 'Changer IP', font=FONT, fill=255)
            draw.text((XABSCISSA, line(2)), 'Univers', font=FONT, fill=255)
            draw.text((XABSCISSA, line(3)), 'Changer univers', font=FONT, fill=255)
            draw.text((XABSCISSA, line(4)), 'Eteindre', font=FONT, fill=255)
        
        # Changement de l'adresse IP
        elif self.index == 2:
            draw.text((XABSCISSA, line(0)), 'Adresse IP', font=FONT, fill=255)
            draw.text((XABSCISSA, line(1)), '> Changer IP', font=FONT, fill=255)
            draw.text((XABSCISSA, line(2)), 'Univers', font=FONT, fill=255)
            draw.text((XABSCISSA, line(3)), 'Changer univers', font=FONT, fill=255)
            draw.text((XABSCISSA, line(4)), 'Eteindre', font=FONT, fill=255)
        
        # Affichage de l'univers
        elif self.index == 3:
            draw.text((XABSCISSA, line(0)), 'Adresse IP', font=FONT, fill=255)
            draw.text((XABSCISSA, line(1)), 'Changer IP', font=FONT, fill=255)
            draw.text((XABSCISSA, line(2)), '> Univers', font=FONT, fill=255)
            draw.text((XABSCISSA, line(3)), 'Changer univers', font=FONT, fill=255)
            draw.text((XABSCISSA, line(4)), 'Eteindre', font=FONT, fill=255)

        # Changement de l'univers
        elif self.index == 4:
            draw.text((XABSCISSA, line(0)), 'Adresse IP', font=FONT, fill=255)
            draw.text((XABSCISSA, line(1)), 'Changer IP', font=FONT, fill=255)
            draw.text((XABSCISSA, line(2)), 'Univers', font=FONT, fill=255)
            draw.text((XABSCISSA, line(3)), '> Changer univers', font=FONT, fill=255)
            draw.text((XABSCISSA, line(4)), 'Eteindre', font=FONT, fill=255)

        # Eteindre
        elif self.index == 5:
            draw.text((XABSCISSA, line(0)), 'Adresse IP', font=FONT, fill=255)
            draw.text((XABSCISSA, line(1)), 'Changer IP', font=FONT, fill=255)
            draw.text((XABSCISSA, line(2)), 'Univers', font=FONT, fill=255)
            draw.text((XABSCISSA, line(3)), 'Changer univers', font=FONT, fill=255)
            draw.text((XABSCISSA, line(4)), '> Eteindre', font=FONT, fill=255)
    

    def getNextPage(self, event):
        # Sélection de la page
        if event == BtnEvent.DOWN:
            self.index += 1
            if self.index == 6:
                self.index = 1

        elif event == BtnEvent.UP:
            self.index -= 1
            if self.index == 0:
                self.index = 5

        # Validation de la page sélectionné
        if self.index == 1 and event == BtnEvent.ENTER:
            return Pages.AFFIP

        elif (self.index == 2 and event == BtnEvent.ENTER):
            sleep(.2)
            return Pages.AIDEIP

        elif (self.index == 3 and event == BtnEvent.ENTER):
            return Pages.AFFUNIVERS

        elif (self.index == 4 and event == BtnEvent.ENTER):
            return Pages.AIDEUNIVERS
            
        elif (self.index == 5 and event == BtnEvent.ENTER):
            return Pages.ETEINDRE

        return Pages.ACCUEIL
