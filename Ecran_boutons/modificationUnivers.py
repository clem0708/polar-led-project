"""Page permettant à l'utilisateur de modifier l'univers du Raspberry
Cette page apparait une fois que l'utilisateur a fini de lire le fichier d'aide sur la modification de l'univers
"""

from sys import path
from time import sleep
from os import system

from abstractPage import AbstractPage
from classPages import Pages
from classBtnEvent import BtnEvent

path.append('/home/pi/polar-led-project/djangotest/djangotest/Fichier_commun')

from functions import line, formatUnivers, affValSet, valideUnivers
from constante import XABSCISSA, FONT

class ModificationUnivers(AbstractPage):
    
    # Déclaration des variables
    val = 0
    index = 1
    verrou = False

    # Liste contenant la valeur de l'univers
    # Univers par défaut : 00000
    listUni = [0, 0, 0, 0, 0]

    def render(self, draw):
        
        # Affichage de l'univers par défaut
        formatUnivers(draw, self.listUni)

        # Mode sélection de champ
        if self.verrou == False:
            # Ajout d'un repère visuel sous le champ que l'on souhaite sélectionner
            if self.index == 1:
                draw.text((XABSCISSA, line(1)), '^', font=FONT, fill=255)
                draw.text((XABSCISSA, line(3)), 'Valider', font=FONT, fill=255)
                draw.text((XABSCISSA, line(4)), "Retour a l'aide", font=FONT, fill=255)
            
            elif self.index == 2:
                draw.text((XABSCISSA+6, line(1)), '^', font=FONT, fill=255)
                draw.text((XABSCISSA, line(3)), 'Valider', font=FONT, fill=255)
                draw.text((XABSCISSA, line(4)), "Retour a l'aide", font=FONT, fill=255)

            elif self.index == 3:
                draw.text((XABSCISSA+12, line(1)), '^', font=FONT, fill=255)
                draw.text((XABSCISSA, line(3)), 'Valider', font=FONT, fill=255)
                draw.text((XABSCISSA, line(4)), "Retour a l'aide", font=FONT, fill=255)

            elif self.index == 4:
                draw.text((XABSCISSA+18, line(1)), '^', font=FONT, fill=255)
                draw.text((XABSCISSA, line(3)), 'Valider', font=FONT, fill=255)
                draw.text((XABSCISSA, line(4)), "Retour a l'aide", font=FONT, fill=255)

            elif self.index == 5:
                draw.text((XABSCISSA+24, line(1)), '^', font=FONT, fill=255)
                draw.text((XABSCISSA, line(3)), 'Valider', font=FONT, fill=255)
                draw.text((XABSCISSA, line(4)), "Retour a l'aide", font=FONT, fill=255)
            
            elif self.index == 6:
                draw.text((XABSCISSA, line(3)), '> Valider', font=FONT, fill=255)
                draw.text((XABSCISSA, line(4)), "Retour a l'aide", font=FONT, fill=255)

            elif self.index == 7:
                draw.text((XABSCISSA, line(3)), 'Valider', font=FONT, fill=255)
                draw.text((XABSCISSA, line(4)), "> Retour a l'aide", font=FONT, fill=255)

        # Mode édition de champ
        elif self.verrou == True:
            # Affichage de la nouvelle valeur pour le champ sélectionné
            affValSet(draw, self.val, self.index)
    
    def getNextPage(self, event):
        
        # Retour à l'accueil en ignorant les modifications
        if event == BtnEvent.MENU:
            self.val = 0
            self.index = 1
            self.verrou = False
            self.listUni = [0, 0, 0, 0, 0]
            return Pages.ACCUEIL

        # ----------------------Mode sélection-------------------------
        elif self.verrou == False:
        
            # Changement de mode (sélection vers édition)
            if (self.index >= 1 and self.index <= 5 ) and event == BtnEvent.ENTER:

                self.verrou = True
                self.val =self.listUni[self.index - 1]
                sleep(.2)

            # Sélection du champ
            elif event == BtnEvent.UP:
                self.index -= 1

                if self.index == 0:
                    self.index = 7

            elif event == BtnEvent.DOWN:
                self.index += 1

                if self.index == 8:
                    self.index = 1
            
            # Application des changements
            elif self.index == 6 and event == BtnEvent.ENTER:
                valideUnivers(self.listUni)
                self.val = 0
                self.index = 1
                self.verrou = False
                self.listUni = [0, 0, 0, 0, 0]
                system("sudo systemctl restart screen.service")

            # Retour à la page d'aide
            elif self.index == 7 and event == BtnEvent.ENTER:
                return Pages.AIDEUNIVERS

        # ---------------------Mode édition---------------------------
        elif self.verrou == True:
            
            # Changement de mode (édition vers sélection)
            if event == BtnEvent.ENTER:
                self.listUni[self.index - 1] = self.val
                self.verrou = False
                sleep(.2)
            
            # Modification de la valeur du champ sélectionné
            elif event == BtnEvent.UP:
                self.val -= 1

                if self.val == -1:
                    self.val = 9
            
            elif event == BtnEvent.DOWN:
                self.val += 1

                if self.val == 10:
                    self.val = 0
        
        return Pages.MODIFUNIVERS