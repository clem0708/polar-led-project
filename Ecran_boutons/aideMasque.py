"""Page affichant le fichier d'aide pour la modification du masque
Cette page apparait une fois que l'utilisateur a modifier l'adresse IP du Raspberry
"""

from sys import path

from abstractPage import AbstractPage
from classPages import Pages
from classBtnEvent import BtnEvent

path.append('/home/pi/polar-led-project/djangotest/djangotest/Fichier_commun')

from functions import line
from constante import XABSCISSA, FONT

class AideMasque(AbstractPage):

    index = 1

    def render(self, draw):
        if self.index == 1:
            draw.text((XABSCISSA, line(0)), ' Changement Masque', font=FONT, fill=255)
            draw.text((XABSCISSA, line(1)), "Fichier d'aide pour", font=FONT, fill=255)
            draw.text((XABSCISSA, line(2)), "le masque", font=FONT, fill=255)
            draw.text((XABSCISSA, line(4)), '      Page 1/3', font=FONT, fill=255)
            draw.text((XABSCISSA, line(5)), '<-Haut          Bas->', font=FONT, fill=255)

        elif self.index == 2:
            draw.text((XABSCISSA, line(0)), ' Changement Masque', font=FONT, fill=255)
            draw.text((XABSCISSA, line(1)), 'Haut/Bas: selection', font=FONT, fill=255)
            draw.text((XABSCISSA, line(2)), 'de la valeur', font=FONT, fill=255)
            draw.text((XABSCISSA, line(4)), '      Page 2/3', font=FONT, fill=255)
            draw.text((XABSCISSA, line(5)), '<-Haut          Bas->', font=FONT, fill=255)

        elif self.index == 3:
            draw.text((XABSCISSA, line(0)), ' Changement Masque', font=FONT, fill=255)
            draw.text((XABSCISSA, line(1)), 'Entrer: validation', font=FONT, fill=255)
            draw.text((XABSCISSA, line(2)), 'du choix', font=FONT, fill=255)
            draw.text((XABSCISSA, line(4)), '      Page 3/3', font=FONT, fill=255)
            draw.text((XABSCISSA, line(5)), '<-Haut          Bas->', font=FONT, fill=255)
        

    def getNextPage(self, event):
        # Changement de page
        if event == BtnEvent.DOWN:
            self.index += 1

            if self.index == 4:
                self.index = 1
                return Pages.MODIFMASQUE
        
        elif event == BtnEvent.UP:
            self.index -= 1

            if self.index == 0:
                self.index = 1
                return Pages.MODIFIP
        
        # Retour à l'accueil
        elif event == BtnEvent.MENU:
            self.index = 1
            return Pages.ACCUEIL
        
        return Pages.AIDEMASQUE