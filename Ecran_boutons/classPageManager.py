"""Cette classe permet de faire le lien entre toute les différentes pages du programme
"""

from sys import path

# Module externe à Python (installation requise)
# Librairies utilisées pour l'écran
import Adafruit_SSD1306
# Librairie utilisé pour les boutons
from gpiozero import Button
# Librairies utilisées pour créer du contenu sur l'écran
from PIL import Image, ImageDraw

from classBtnEvent import BtnEvent
from classPages import Pages

# Pages générales
from accueil import Accueil
from eteindre import Eteindre
from initData import InitData

# Pages en lien avec l'adresse IP
from affichageIP import AffichageIP
from aideIP import AideIP
from modificationIP import ModificationIP
from aideMasque import AideMasque
from modificationMasque import ModificationMasque
from confirmeModification import ConfirmeModification

# Pages en lien avec l'univers
from affichageUnivers import AffichageUnivers
from aideUnivers import AideUnivers
from modificationUnivers import ModificationUnivers

path.append('/home/pi/polar-led-project/djangotest/djangotest/Fichier_commun')

from functions import menu

class PageManager:
    # Configuration des boutons
    BMenu = Button(26)
    BEnter = Button(6)
    BUp = Button(16)
    BDown = Button(5)

    #Création d'un écran avec la résolution 128*64 (marche aussi avec 128*32)
    disp = Adafruit_SSD1306.SSD1306_128_64(rst=None)

    # Initialise l'écran puis nettoyage de l'écran
    disp.begin()
    disp.clear()
    disp.display()

    # Création d'une image vide et noire pour dessiner dessus
    # La taille est égale aux dimensions en pixels de l'écran utilisé
    # Faire attention à bien créer l'image avec le mode '1' pour 1-bit color (noir ou blanc)
    width = disp.width
    height = disp.height
    image = Image.new('1', (width, height))

    # Création d'un objet de dessin
    draw = ImageDraw.Draw(image)

    # Dessine un rectangle noir qui rempli l'objet 'image'
    draw.rectangle((0,0,width,height), outline=0, fill=0)

    # Définition de quelques constantes afin de faciliter la manipulation des formes
    padding = -2
    top = padding
    bottom = height-padding
    # Création d'un point de repère afin d'avoir la position sur l'axe x
    x = 0
    
    pages = {
        # Pages générales
        Pages.ACCUEIL : Accueil(),
        Pages.ETEINDRE : Eteindre(),
        Pages.INITDATA : InitData(),

        # Page en lien avec l'adresse IP
        Pages.AFFIP : AffichageIP(),
        Pages.AIDEIP : AideIP(),
        Pages.MODIFIP : ModificationIP(),
        Pages.AIDEMASQUE : AideMasque(),
        Pages.MODIFMASQUE : ModificationMasque(),
        Pages.CONFMODIF : ConfirmeModification(),

        # Page en lien avec l'univers
        Pages.AFFUNIVERS : AffichageUnivers(),
        Pages.AIDEUNIVERS : AideUnivers(),
        Pages.MODIFUNIVERS : ModificationUnivers()
    }

    currentPage = pages[Pages.INITDATA]

    def renderloop(self):
        
        # Affichage du menu
        menu(self.draw)

        while True:
            self.draw.rectangle((0,0,self.width, self.top+50), outline=0, fill=0)

            # Assimilation des boutons avec leur énumération
            currentEvent = BtnEvent.NONE
            # Bouton Menu
            if (self.BMenu.is_pressed):
                currentEvent = BtnEvent.MENU
            # Bouton Entrer
            elif (self.BEnter.is_pressed):
                currentEvent = BtnEvent.ENTER
            # Bouton Haut
            elif (self.BUp.is_pressed):
                currentEvent = BtnEvent.UP
            # Bouton Bas
            elif (self.BDown.is_pressed):
                currentEvent = BtnEvent.DOWN

            # Réalisation d'une action propre à la page actuelle en fonction 
            # du bouton qui a été pressé (changement de contenu, changement de page)
            nextPage = self.currentPage.getNextPage(currentEvent)
            currentPage = self.pages[nextPage]
            self.currentPage.render(self.draw)
            
            # Affichage du contenu sur l'écran
            self.disp.image(self.image)
            self.disp.display()

            # Rédéfinition de la page actuelle
            self.currentPage = currentPage



# Création d'un objet PageManager qui va permettre à l'utilisateur de naviguer 
# entre les différentes pages du programme
pageManager = PageManager()

# Affichage de la page par défaut (page d'accueil)
pageManager.renderloop()