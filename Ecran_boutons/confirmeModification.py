"""Cette page permet de confirmer ou non les modifications apporté à l'adresse IP
Elle apparait une fois que les modifications du masque ont été validé
"""

from sys import path
from os import system
from time import sleep

from abstractPage import AbstractPage
from classPages import Pages
from classBtnEvent import BtnEvent

path.append('/home/pi/polar-led-project/djangotest/djangotest/Fichier_commun')

from functions import line
from constante import XABSCISSA, FONT

class ConfirmeModification(AbstractPage):

    index = 1
    shutdown = 0

    def render(self, draw):
        if self.index == 1:
            draw.text((XABSCISSA, line(0)), 'Appliquer les', font=FONT, fill=255)
            draw.text((XABSCISSA, line(1)), 'modifications ?', font=FONT, fill=255)
            draw.text((XABSCISSA, line(2)), '> Non (Accueil)', font=FONT, fill=255)
            draw.text((XABSCISSA, line(3)), 'Oui (Redemmarage)', font=FONT, fill=255)

        # Application des changements à l'aide d'un script qui va 
        # copier coller le nouveau fichier de configuration au bon emplacement.
        elif self.index == 2:
            draw.text((XABSCISSA, line(0)), 'Appliquer les', font=FONT, fill=255)
            draw.text((XABSCISSA, line(1)), 'modifications ?', font=FONT, fill=255)
            draw.text((XABSCISSA, line(2)), 'Non (Accueil)', font=FONT, fill=255)
            draw.text((XABSCISSA, line(3)), '> Oui (Redemarrage)', font=FONT, fill=255)

        elif self.shutdown == 1:
            draw.text((XABSCISSA,line(0)), 'Redemarrage en cours', font=FONT, fill=255)
            draw.text((XABSCISSA,line(1)), 'Patientez...', font=FONT, fill=255)
            self.shutdown = 1
            
    def getNextPage(self, event):
        # Sélection du choix
        if event == BtnEvent.DOWN:
            self.index += 1
            if self.index == 3:
                self.index = 1

        elif event == BtnEvent.UP:
            self.index -= 1
            if self.index == 0:
                self.index = 2

        # Validation du choix sélectionné
        if (self.index == 1 and event == BtnEvent.ENTER):
            return Pages.ACCUEIL

        # Application des changements + redemmarage
        elif (self.index == 2 and event == BtnEvent.ENTER):
            self.index = 0
            self.shutdown = 1

        elif self.shutdown == 1:
            sleep(0.5)
            system('sudo sh /home/pi/polar-led-project/changeIP.sh')

        # Retour au menu
        elif event == BtnEvent.MENU:
            return Pages.ACCUEIL

        return Pages.CONFMODIF