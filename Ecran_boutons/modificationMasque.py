"""Page permettant à l'utilisateur de modifier le masque de l'adresse IP
Cette page apparait une fois que l'utilisateur a fini de lire le fichier d'aide sur la modification du masque
"""

from sys import path
from time import sleep

from abstractPage import AbstractPage
from classPages import Pages
from classBtnEvent import BtnEvent

path.append('/home/pi/polar-led-project/djangotest/djangotest/Fichier_commun')

from functions import line
from constante import XABSCISSA, FONT

class ModificationMasque(AbstractPage):

    masque = 24

    def render(self, draw):

        if self.masque == 25:
            draw.text((XABSCISSA, line(0)), "Masque de l'IP :", font=FONT, fill=255)
            draw.text((XABSCISSA, line(1)), '/'+str(self.masque - 1), font=FONT, fill=255)
            draw.text((XABSCISSA, line(3)), "> Retour a l'aide", font=FONT, fill=255)
            draw.text((XABSCISSA, line(4)), '"Entrer" pour valider', font=FONT, fill=255)

        else:
            draw.text((XABSCISSA, line(0)), "Masque de l'IP :", font=FONT, fill=255)
            draw.text((XABSCISSA, line(1)), '/'+str(self.masque), font=FONT, fill=255)
            draw.text((XABSCISSA, line(3)), "Retour a l'aide", font=FONT, fill=255)
            draw.text((XABSCISSA, line(4)), '"Entrer" pour valider', font=FONT, fill=255)



    def getNextPage(self, event):
        
        # Applications des chagements
        if self.masque <= 24 and event == BtnEvent.ENTER:
            with open('/home/pi/polar-led-project/interfaces', 'a+') as file:
                    file.write("netmask " + str(self.masque))
            self.masque = 24
            sleep(.2)
            return Pages.CONFMODIF

        # Retour à l'aide
        if (self.masque == 25 and event == BtnEvent.ENTER):
            return Pages.AIDEMASQUE

        # Retour à l'acceuil
        elif event == BtnEvent.MENU:
            self.masque = 24
            return Pages.ACCUEIL
        
        # Modification du masque
        if event == BtnEvent.UP:
            self.masque -= 1
            if self.masque == 0:
                self.masque = 25

        elif event == BtnEvent.DOWN:
            self.masque += 1
            if self.masque == 26:
                self.masque = 1

        return Pages.MODIFMASQUE
