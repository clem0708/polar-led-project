"""Page n'affichant aucun contenu mais servant au démarrage afin de remplir le fichier JSON contenant les informations suivantes :
    - Adresse IP
    - Univers
""" 

from sys import path
from json import dump, load
from time import sleep

from abstractPage import AbstractPage
from classPages import Pages

path.append('/home/pi/polar-led-project/djangotest/djangotest/Fichier_commun')

from functions import getIp
from constante import DATA

class InitData(AbstractPage):

    def render(self, draw):

        sleep(3)
        ip = getIp()

        # Ouverture du fichier data.json
        with open(DATA, 'r') as file:
            data = load(file)

        # Attribution d'une valeur au champ "ip"
        data["ip"] = ip

        # Attribution d'une valeur au champ "univers"
        # S'il est vide alors "univers" = 1
        if data["univers"] == "" :
            data["univers"] = "1"

        else:
            pass

        # Copie des modifications dans le fichier
        with open(DATA, 'w+') as file:
            dump(data, file, indent=4)


    def getNextPage(self, event):
        return Pages.ACCUEIL
