"""Classe abstraite utilisée pour créer les différentes pages qui seront accessibles sur l'écran LCD
"""

from abc import ABC, abstractmethod

class AbstractPage(ABC):

    def __init__(self):
        super().__init__()

    @abstractmethod
    def render(self, draw):
        pass

    @abstractmethod
    def getNextPage(self, event):
        pass