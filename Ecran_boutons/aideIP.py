"""Page affichant le fichier d'aide pour la modification de l'adresse IP
Cette page apparait une fois que l'utilisateur a sélectionné l'intreface à modifier
"""
from sys import path

from abstractPage import AbstractPage
from classPages import Pages
from classBtnEvent import BtnEvent

path.append('/home/pi/polar-led-project/djangotest/djangotest/Fichier_commun')


from functions import line
from constante import XABSCISSA, FONT

class AideIP(AbstractPage):

    index = 1

    def render(self, draw):
        if self.index == 1:
            draw.text((XABSCISSA, line(0)), '   Changement IP', font=FONT, fill=255)
            draw.text((XABSCISSA, line(1)), "Fichier d'aide", font=FONT, fill=255)
            draw.text((XABSCISSA, line(2)), "pour l'IP", font=FONT, fill=255)
            draw.text((XABSCISSA, line(4)), '      Page 1/5', font=FONT, fill=255)
            draw.text((XABSCISSA, line(5)), '<-Haut          Bas->', font=FONT, fill=255)
        
        elif self.index == 2:
            draw.text((XABSCISSA, line(0)), '   Mode selection', font=FONT, fill=255)
            draw.text((XABSCISSA, line(1)), 'Haut/Bas: selection', font=FONT, fill=255)
            draw.text((XABSCISSA, line(2)), 'du champ', font=FONT, fill=255)
            draw.text((XABSCISSA, line(4)), '      Page 2/5', font=FONT, fill=255)
            draw.text((XABSCISSA, line(5)), '<-Haut          Bas->', font=FONT, fill=255)

        elif self.index == 3:
            draw.text((XABSCISSA, line(0)), '   Mode selection', font=FONT, fill=255)
            draw.text((XABSCISSA, line(1)), 'Entrer: selection', font=FONT, fill=255)
            draw.text((XABSCISSA, line(2)), 'du champ', font=FONT, fill=255)
            draw.text((XABSCISSA, line(4)), '      Page 3/5', font=FONT, fill=255)
            draw.text((XABSCISSA, line(5)), '<-Haut          Bas->', font=FONT, fill=255)        

        elif self.index == 4:
            draw.text((XABSCISSA, line(0)), '    Mode edition', font=FONT, fill=255)
            draw.text((XABSCISSA, line(1)), 'Haut/Bas: selection', font=FONT, fill=255)
            draw.text((XABSCISSA, line(2)), 'de la valeur', font=FONT, fill=255)
            draw.text((XABSCISSA, line(4)), '      Page 4/5', font=FONT, fill=255)
            draw.text((XABSCISSA, line(5)), '<-Haut          Bas->', font=FONT, fill=255)

        elif self.index == 5:
            draw.text((XABSCISSA, line(0)), '    Mode edition', font=FONT, fill=255)
            draw.text((XABSCISSA, line(1)), 'Entrer: valider la', font=FONT, fill=255)
            draw.text((XABSCISSA, line(2)), 'modification', font=FONT, fill=255)
            draw.text((XABSCISSA, line(4)), '      Page 5/5', font=FONT, fill=255)
            draw.text((XABSCISSA, line(5)), '<-Haut          Bas->', font=FONT, fill=255)
        

    def getNextPage(self, event):
        # Changement de page
        if event == BtnEvent.DOWN:
            self.index += 1

            if self.index == 6:
                self.index = 1
                return Pages.MODIFIP
        
        elif event == BtnEvent.UP:
            self.index -= 1

            if self.index == 0:
                self.index = 1
                return Pages.INTERFACE
        
        # Retour à l'accueil
        elif event == BtnEvent.MENU:
            self.index = 1
            return Pages.ACCUEIL
        
        return Pages.AIDEIP