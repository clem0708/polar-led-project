"""Cette classe permet d'associer un bouton déclarer et instancier dans le Page Manager avec une énumération
"""

from enum import Enum

class BtnEvent(Enum):
    NONE = 0
    MENU = 1
    ENTER = 2
    UP = 3
    DOWN = 4