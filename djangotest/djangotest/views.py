from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
import os
from json import dump, load
import json
from django.views.decorators.http import require_http_methods
import requests
from ipaddress import IPv4Address
import djangotest.Fichier_commun.functions as functions
import djangotest.Fichier_commun.constante as const
import time



# Définition d'une fonction home
def home(request):
    # Prend le fichier dans template
    template = loader.get_template('body.html')
    # Appelé body.html
    return HttpResponse(template.render())
    # Renvoi la page web en réponse


class Eteindre(View): #ressource de l'api pour éteindre le raspberry
    def post(self, request):
        cmd1 = "sudo shutdown -h now" 
        #commande pour éteindre le Raspberry
        os.system(cmd1)
        #dit au systeme du Raspberry d'exécuté la commande
        html = """
        Raspberry éteinte
        """
        return HttpResponse(html) #réponse http perçu

class Test(View): #ressource du raspberry pour tester le ruban led
    def post(self, request):
        cmd6 = "sh test.sh"
        #commande qui exécute le script test.sh
        os.system(cmd6)
        #exécute directement sur le système du Raspberry la commande
        html = """
        Eclairage LED
        """
        return HttpResponse(html) #réponse http perçu

class IpUnivers(View): #ressource du raspberry pour la configurationd e l'univers et de l'ip
    def post(self, request):
        data = json.loads(request.body)#parse, décode le json envoyer dans la requete par la fonction fetch() en javacscript
        with open(const.DATA, mode='w+') as file: #ouvr ele document en mode éditeur et écrase les données d'avant
            file.write(str(request.body, 'UTF-8')) #écrit le contenu de la requete envoyer dans le fichier json
            
        with open(const.DATA, 'r') as file: #ouvre le document data.jon en mdoe lecture
            data = load(file) #charge le document dans la variable data
            ip = data["ip"] #définition de la variable ip en isolant le contenu json
            univers = data["univers"] #définition de la variable univers en isolant le contenu json
            # check(ip)
            try:
                IPv4Address(ip)#regarde si c'est bien une adresse de type ipv4
            except: #sinon bloque l'envoie et met une erreur 400 en reponse html
                html="""
                400
                """
                return HttpResponse(html)
            try:
                y = json.loads(univers) #parse, décode le json univers
            except: #si ca marche pas alors il y a une erreur 400 retourner
                html="""
                400
                """
                return HttpResponse(html)
            time.sleep(10) #attend 10 secondes avant d'exécuté la prochaine fonction
            functions.valideIpIhm(ip) #éxécute la fonction valideIpIhm qui change l'ip du Raspberry et redémarre celui-ci
        #variable
        html="""
        Données envoyées dans data.json
        """
        return HttpResponse(html) #si tout ce passe bien c'est la réponse html renvoyé
        
