from PIL import ImageFont

# ----------------------------- CONSTANTE UTILISÉ PAR L'ECRAN -------------------------------------

# Configuration des pins
RST = None

# En-tête du fichier de configuration de l'IP
HEAD = "# This file describes the network interfaces available on your system \n# and how to activate them. For more information, see interfaces(5).\nsource /etc/network/interfaces.d/*\n\n# The loopback network interface\nauto lo\niface lo inet loopback\n"

XABSCISSA = 0
PADDINGTOP = 0
LINEHEIGHT = 8
FONT = ImageFont.load_default()

# Fichier contenant l'adresse IP et l'univers du raspberry
DATA = "/home/pi/polar-led-project/djangotest/djangotest/Fichier_commun/data.json"

# ----------------------------- CONSTANTE UTILISÉ PAR LE RUBAN -------------------------------------

LED_COUNT      = 60 # Nombre total de LED disponible sur le ruban
LED_PIN        = 18 # Correspond au numéro du PIN utilisé pour transmettre les données (GPIO 18)
LED_FREQ_HZ    = 800000 # Fréquence des LED en Hertz
LED_DMA        = 10 # Canal utilisé pour générer le signal
LED_BRIGHTNESS = 255 # Intensité des LED (0 = sombre, 255 = lumineux)
LED_INVERT     = False # Inverse le signal (True pour inversé)
LED_CHANNEL    = 0 # Mettre à '1' pour les GPIOs 13, 19, 41, 45 ou 53