from functions import *
from constante import *
import time

strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
strip.begin()

try:
    while True: #boucle qui permet de coloré le ruban en blanc
        colorWipe(strip, Color(255,255,255)) #couleur blanche
        time.sleep(5) #timeur de 5 secondes pour le teste du ruban led
        colorWipe(strip, Color(0,0,0)) #après le timeur extinction des led
        break #sort de la boucle


except KeyboardInterrupt: #si il y a une erreur alors ne pas démarré les led
    colorWipe(strip, Color(0,0,0))
