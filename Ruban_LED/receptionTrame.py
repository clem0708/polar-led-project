from sys import path

from rpi_ws281x import *

path.append('/home/pi/polar-led-project/djangotest/djangotest/Fichier_commun')

from functions import demarrageServeur,  colorWipe
from constante import *

try:
	strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
	strip.begin()
	demarrageServeur(strip)

except KeyboardInterrupt:
	colorWipe(strip, Color(0,0,0))