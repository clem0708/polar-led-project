import time
from rpi_ws281x import *
import argparse

LED_COUNT      = 60    # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53

ROUGE = Color(255, 0, 0)
VERT = Color(0, 255, 0)
BLEU = Color(0, 0, 255)
JAUNE = Color(255, 255, 0)
CYAN = Color(0, 255, 255)
MAGENTA = Color(255, 0, 255)
BLANC = Color(255, 255, 255)

def test(strip, color, wait_ms = 50):
	for i in range(strip.numPixels()//2):
		strip.setPixelColor(i*2, color)
		strip.show()
		time.sleep(wait_ms/1000.0)

	for i in range(strip.numPixels()//2):
		strip.setPixelColor(i*2-1, color)
		strip.show()
		time.sleep(wait_ms/1000.0)


if __name__ == '__main__':
    # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--clear', action='store_true', help='clear the display on exit')
    args = parser.parse_args()

    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
    # Intialize the library (must be called once before other functions).
    strip.begin()

    print ('Press Ctrl-C to quit.')
    if not args.clear:
        print('Use "-c" argument to clear LEDs on exit')


    try:

        while True:
            test(strip, ROUGE)
            test(strip, VERT)
            test(strip, BLEU)
            test(strip, CYAN)
            test(strip, JAUNE)
            test(strip, MAGENTA)
            test(strip, BLANC)

    except KeyboardInterrupt:
        if args.clear:
            test(strip, Color(0,0,0), 10)
