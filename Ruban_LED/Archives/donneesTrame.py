""" ---------------------------------------------------------------------------------------------
Nom : Didier Maxime et François-Xavier
Date : 26/01/2021

ce bout de code va servir a savoir lire, traduire une trame Art-Net pour ensuite envoyer l'information,
en fonction de la trame, au LED.
"""

from sys import path

from rpi_ws281x import *#import de la bibliothèque rpi_ws281x

path.append('../../../Fichier_commun')
path.append('/home/pi/polar-led-project/djangotest/Fichier_commun')


LED_COUNT      = 60
LED_PIN        = 18
LED_FREQ_HZ    = 800000
LED_DMA        = 10
LED_BRIGHTNESS = 255
LED_INVERT     = False
LED_CHANNEL    = 0


def colorisation(strip, couleur, led):
	"""

	:param strip: objet qui a pour paramètre 7 fonctions différentes
	:param couleur: comme l'indique le nom la couleur
	:param led: quel led est séléctionner pour cette tache

	cette fonction va servir pour la coloration des led
	"""
	strip.setPixelColor(led, couleur) #séléctionne la led avec la couleur voulu, ca enregistre l'état d'une led
	strip.show() #va prendre l'enregistrement et l'appliquer sur la ou les leds



strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
"""
adafruit_neopixel est un objet ayant en paramètre toute l'initialisation au début c'est a dire,
le numéro de la led : LED_COUNT
a quel pin de la led on est : LED_PIN
a quel fréquence on veut la led : LED_FREQ_HZ
permet d'utiliser le signal généré : LED_DMA
quel univers on va utiliser : LED_CHANNEL
"""
strip.begin() #commence l'interprétation


trame = "64 ed 19 36 19 36 02 la df 30 41 72 74 2d 4e 65 74 00 00 50 00 0e 3d 00 00 00 02 00" \
		" ff 00 00 00 ff 00 00 00 ff 00 00 00 ff ff ff ff ff 00 ff 00 ff 00 ff ff ed ac 01 00 00" \
		" ff ff ff ff ff 00 00" #exemple de trame Art-Net que l'on peut recevoir

tableauTrame = trame.split(" ") #répertorie dans un tableau
tableauDonnees = tableauTrame[28:] #le tableau de donnée c'est le tableau trame avec le debut des données (28)

i = 0
led = 0
while(i < len(tableauDonnees)):
	"""
	tant que i inf a la longueur du tableau de donnée
	alors il faut le parcourir et interpréter toute les données jusqu'à la fin
	"""
	couleur = Color(int(tableauDonnees[i], 16), int(tableauDonnees[i + 1], 16), int(tableauDonnees[i + 2], 16))
	colorisation(strip, couleur, led)
	i = i + 3
	led = led + 1
