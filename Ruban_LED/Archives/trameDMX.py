from ArtNet.fonctions import *
from ArtNet.variables import *

debutDonnee = 0
trame = [0xb8, 0x27, 0xeb, 0xc5, 0xbb, 0x74, 0x80, 0x30, 0x49, 0x4a, 0x51, 0x5f, 0xc0, 0xa8, 0xc8, 0x02, 0xc0, 0xa8, 0x00, 0x65, 0x08, 0x00, \
        0x19, 0x36, 0x19, 0x36, 0x02, 0x1a, 0xdf, 0x30, 0x41, 0x72, 0x74, 0x2d, 0x4e, 0x65, 0x74, 0x50, 0x00, 0x0e, 0x3d, 0x00, 0x00, 0x01, 0x02, 0x00, \
        0xff, 0x00, 0x00, 0x00, 0xff, 0x00, 0x00, 0x00, 0xff, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0xff, 0x00, 0xff, 0x00, 0xff, 0xff, \
    	0xed, 0xac, 0x01, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0xff, 0x00, 0xaa, 0x45, 0x25, 0xff, 0x20, 0xf4, 0xa5, 0xff, 0xff, 0xee]

strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
strip.begin()

donnees = trame[46 :]

try:
    while True:
        etudeTrame(strip, donnees)
        
except KeyboardInterrupt:
    colorWipe(strip, Color(0,0,0))
