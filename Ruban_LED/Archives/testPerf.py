
""" ---------------------------------------------------------------------------------------------
Nom : Didier Maxime et François-Xavier
Date : 02/02/2021

ce bout de code va servir a savoir lire, traduire une trame Art-Net pour ensuite envoyer l'information,
en fonction de la trame, au LED.
"""

"""
importation des bibliothèque rpi_ws281x, argparse, time car elle va nous etre utile pour interpréter la trame et utiliser des fonctions ce trouvant dans
celle-ci
"""
from rpi_ws281x import *#import de la bibliothèque rpi_ws281x
import argparse
import time


LED_COUNT      = 60 #le nombre de LED présente sur la barre
LED_PIN        = 18 #le numéro de la LED
LED_FREQ_HZ    = 800000 #la fréquence de la LED
LED_DMA        = 10
LED_BRIGHTNESS = 255
LED_INVERT     = False
LED_CHANNEL    = 0

"""
fonction ColorWipe avec en paramètre strip(qui va permettre d'enlever les valeurs
nulle ou que l'on ne désire pas

pour un i  à porter d'un numéro de LED
	séléctionne le i à porter d'un numéro et séléctionne la couleur
	affiche le résultat
"""

def colorWipe(strip, color):
    """Wipe color across display a pixel at a time."""
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)
        strip.show()

"""
fonction pour la colorisation avec en paramètre la couleur le numéro de la LED
tous cela avec le strip qui permet d'enlever les "mauvaises herbes"
	il faut séléctionner la LED et ca couleur
	afficher
"""

def colorisation(strip, couleur, led):
	strip.setPixelColor(led, couleur) #séléctionne la led avec la couleur voulu, ca enregistre l'état d'une led
	strip.show() #va prendre l'enregistrement et l'appliquer sur la ou les leds

"""
fonction allumage avec en paramètre le tableau et l'interprétation strip pour enlever
ce que l'on ne veux pas

variable i initialiser
variable LED initialiser

pour tout i inférieur a la longueur du tableau
	la couleur sera égale a la 16ieme valeur du tableau ensuite incrémente le i
	de 1 et prend la 16ième valeur et on incrémente encore le i de 1 et on prend la
	16ième valeur
	on vient utiliser la fonction colorisation
	le i on l'incrémente de 3
	on incrémente de 1 la variable LED
"""


def allumage(tableau, strip):
	i = 0
	led = 0
	while(i < len(tableau)):
		couleur = Color(int(tableau[i], 16), int(tableau[i + 1], 16), int(tableau[i + 2], 16))
		colorisation(strip, couleur, led)
		i = i + 3
		led = led + 1

"""
la fonction main pour l'éxécutable, l'endroit ou il y a tous les procéder
d'argument
"""

if __name__ == '__main__':
	# Process arguments
	parser = argparse.ArgumentParser()
	parser.add_argument('-c', '--clear', action='store_true', help='clear the display on exit')
	args = parser.parse_args()

	strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
	strip.begin() #commence l'interprétation


	trame1 = "64 ed 19 36 19 36 02 la df 30 41 72 74 2d 4e 65 74 00 00 50 00 0e 3d 00 00 00 02 00" \
			" ff 00 00 00 ff 00 00 00 ff 00 00 00 ff ff ff ff ff 00 ff 00 ff 00 ff ff ed ac 01 00 00" \
			" ff ff ff ff ff 00 00" #exemple de trame Art-Net que l'on peut recevoir

	trame2 = "64 ed 19 36 19 36 02 la df 30 41 72 74 2d 4e 65 74 00 00 50 00 0e 3d 00 00 00 02 00" \
			" ac 12 10 00 af 22 ff b0 00 00 50 77 ff 00 00 ff 00 11 ab 00 ff 00 ff ff ed ac 01 00 00" \
			" ff 44 ff ff 22 00 00 ff ff ff 00 ff ff"

	tableauTrame1 = trame1.split(" ") #répertorie dans un tableau
	tableauDonnees1 = tableauTrame1[28:] #le tableau de donnée c'est le tableau trame avec le debut des données (28)

	tableauTrame2 = trame2.split(" ") #répertorie dans un tableau
	tableauDonnees2 = tableauTrame2[28:] #le tableau de donnée c'est le tableau trame avec le debut des données (28)

	print ('Press Ctrl-C to quit.') #affiche comment quitter la commande
	if not args.clear: #si l'arguement clear ne marche pas
		print('Use "-c" argument to clear LEDs on exit')

"""
exception pour le controle d'erreur
pour tout vrai
	temps1 sera le renvoie de la valeur d'un compteur de perf
	va etre utile pour mesurer une courte durée
	utiliser la fonction allumage avec en paramètre le tableau de donnée
	temps 2 sera aussi un renvoi de valeur d'un compteur de perf
	utiliser a nouveau l'allumage mais cette fois ci avec en paramètre le tableau de donnée2
	temps 3 sera aussi un renvoie de la valeur d'un compteur de perf
	afficher ensuite le temps 3 - le temps 1
"""
try:
	while True:
		temps1 = time.perf_counter()
		allumage(tableauDonnees1, strip)
		temps2 = time.perf_counter()
		allumage(tableauDonnees2, strip)
		temps3 = time.perf_counter()
		print(temps3 - temps1)

except KeyboardInterrupt: #exception quand il y a une interruption
	if args.clear:
		colorWipe(strip, Color(0,0,0)) #repart de 0
