from rpi_ws281x import * 
import time


def colorisation(strip, couleur, led):
	strip.setPixelColor(led, couleur)
	strip.show()

def colorWipe(strip, color, wait_ms=50):
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)
        strip.show()
        time.sleep(10/500.0)

def etudeTrame(strip, donnees):
        i = 0
        led = 0
        while(i < len(donnees)):
            color = Color(donnees[i], donnees[i + 1], donnees[i + 2])
            colorisation(strip, color, led)
            i = i + 3
            led = led + 1