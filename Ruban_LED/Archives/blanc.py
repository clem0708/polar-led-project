from sys import path

from ArtNet.fonctions import *
from ArtNet.variables import *

path.append('../../../Fichier_commun')
path.append('/home/pi/polar-led-project/djangotest/Fichier_commun')

strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
strip.begin()

try:
    while True:
        colorWipe(strip, Color(255,255,255))
        
except KeyboardInterrupt:
    colorWipe(strip, Color(0,0,0))