from sys import path
import socket

path.append('/home/pi/polar-led-project/djangotest/djangotest/Fichier_commun')

from rpi_ws281x import *
from functions import colorisation, colorWipe
from constante import *

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

s.bind(("0.0.0.0", 1024))
print("Serveur UDP à l'écoute")


try:
	strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
	strip.begin()

	while(True):
		réception = s.recvfrom(1024)
		messageClient = réception[0]
		addresseClient = réception[1]
		clientMsg = "Message du client: {}".format(messageClient)
		clientIP  = "Adresse IP du client: {}".format(addresseClient)
		print(clientMsg)
		print(clientIP)

		addresseMACSource = messageClient[:5]
		print(addresseMACSource)

		addresseMACPC = messageClient[5:11]
		print(addresseMACPC)

		IPRaspberry = messageClient[11:15]
		print(IPRaspberry)

		IPpc = messageClient[15:19]
		print(IPpc)

		typeIP = messageClient[19:21]
		print(typeIP)

		portEnvoiReception = messageClient[21:25]
		print(portEnvoiReception)

		nbOctetsSuivants = messageClient[25:27]
		print(nbOctetsSuivants)

		octetContrôle = messageClient[27:29]
		print(octetContrôle)

		Artnet = messageClient[29:37]
		print(Artnet)

		opCode = messageClient[37:39]
		print(opCode)

		versionProtocole = messageClient[39:40]
		print(versionProtocole)

		sequence = messageClient[40:41]
		print(sequence)

		sousReseau = messageClient[41:42]
		print(sousReseau)

		univers = messageClient[42:44]
		print(univers)

		taillesDonnées = messageClient[44:46]
		tailleDonnées = tailleDonnées[0]*100 + tailleDonnées[1]
		print(taillesDonnées)

		données = messageClient[46:]
		print(données)

		if((tailleDonnées % 3 == 0) and (len(données) == tailleDonnées)):
			i = 0
			led = 0
			while(i < (len(données) // 3 * 3)):
				couleur = Color(données[i], données[i + 1], données[i + 2])
				colorisation(strip, couleur, led)
				i = i + 3
				led = led + 1
			réponseServeur = str.encode("Réception Trame OK")

		else:
			réponseServeur = str.encode("Trame non conforme")

		s.sendto(réponseServeur, addresseClient)
except KeyboardInterrupt:
	colorWipe(strip, Color(0,0,0))

