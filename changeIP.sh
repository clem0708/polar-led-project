# Suppression du fichier "interfaces" déjà présent
sudo rm /etc/network/interfaces
# Copie du nouveau fichier "interfaces"
sudo cp /home/pi/polar-led-project/interfaces /etc/network/
# Redémarrage du service réseau
sudo systemctl restart networking

sudo reboot